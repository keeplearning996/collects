 **Spring Boot为基础，进行开发FastDFS的客户端，可独立运行的Jar文件。如果您有多个项目（组），并且拥有有文件服务器（FastDFS），那么为了节约其他项目同事的时间成本，这个访问文件服务器（FastDFS）的中间件绝对是个好选择。** 

启动修改端口命令：
    java -Dserver.port=8888 -jar fastDFS-Client.jar 192.168.1.157 22122

 - **福利** 

- spring boot 简单入门可以参考 
-     http://git.oschina.net/keeplearning996/springboot

- 接口使用文档
-     https://my.oschina.net/xwzj/blog/777136

- FastDFS的安装文档
-     https://my.oschina.net/xwzj/blog/760862


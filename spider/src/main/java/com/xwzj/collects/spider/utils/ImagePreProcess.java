package com.xwzj.collects.spider.utils;


import java.io.File;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.sourceforge.tess4j.ITesseract;
import net.sourceforge.tess4j.Tesseract1;
import net.sourceforge.tess4j.TesseractException;

/**
 * 图片验证码处理
 *
 * @author xianfeng.cai
 * @date 2019-03-06
 */
public class ImagePreProcess {

    private static Logger logger = LoggerFactory.getLogger(ImagePreProcess.class);


    /**
     * 验证码解析
     *
     * @param file
     * @return
     */
    public static String validCodeOCR(File file) {
//        Properties properties = System.getProperties();
//        String relativelyPath = properties.getProperty("user.dir");
        ITesseract instance = new Tesseract1();

        // 设置训练库的位置
        String directory = ImagePreProcess.class.getClassLoader().getResource("tessdata").getPath();
        instance.setDatapath(directory);

        instance.setLanguage("eng");//chi_sim ：简体中文， eng	根据需求选择语言库
        String result = null;
        try {
            result = instance.doOCR(file);
        } catch (TesseractException e) {
            logger.error("", e);
        }
        return result;
    }

    public static String validCodeOCR(String filePath) {
        return ImagePreProcess.validCodeOCR(new File(filePath));
    }


    public static void main(String[] args) {

        String result = ImagePreProcess.validCodeOCR("F://jpg//1.jpg");
        System.out.println(result);

    }

}


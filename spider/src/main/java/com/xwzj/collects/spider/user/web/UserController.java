package com.xwzj.collects.spider.user.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.ibatis.annotations.Param;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController  
@EnableAutoConfiguration
@RequestMapping("/user")  
public class UserController {

    @RequestMapping("/user/{myName}")  
    public String demo(HttpServletRequest request,@PathVariable String myName) {  
    	HttpSession session = request.getSession();
		System.out.println("sessionId:"+session.getId());
        return "Hello,"+myName+"!!!";  
    }  
    
    @RequestMapping("/get")  
    public String get(HttpServletRequest request,@Param("name") String name) {  
    	HttpSession session = request.getSession();
		System.out.println("sessionId:"+session.getId());
        return "Hello,"+name+"!!!";  
    }
}

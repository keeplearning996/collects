package com.xwzj.collects.spider;

import org.junit.runner.RunWith;
import org.springframework.boot.test.context.ConfigFileApplicationContextInitializer;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.xwzj.collects.spider.SpiderApplication;


@RunWith(SpringJUnit4ClassRunner.class)  
@ContextConfiguration(classes = SpiderApplication.class,initializers = ConfigFileApplicationContextInitializer.class)
public class ApplicationTest {
   
}

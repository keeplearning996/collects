package com.xwzj.collects.spider.user;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.xwzj.collects.spider.ApplicationTest;
import com.xwzj.collects.spider.user.dao.UserDao;
import com.xwzj.collects.spider.user.entity.User;


public class UserTest extends ApplicationTest{
	
	@Autowired  
    private UserDao userDao;  
      
    @Test  
    public void testInsert(){  
        User user = new User();  
        user.setName("系统管理员");  
        user.setLoginName("admin");
        user.setPassword("admin");
        userDao.insert(user);  
        System.out.println("插入用户信息-->"+user.getName());  
    }  
}
